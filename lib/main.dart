import 'package:flutter/material.dart';
import 'package:flutter/animation.dart';
import 'dart:async';

Future<void> main() async {
  runApp(LogoApp());
  await new Future.delayed(const Duration(seconds : 6));
  runApp(MyApp());
}

class LogoApp extends StatefulWidget {
  _LogoAppState createState() => _LogoAppState();
}

class _LogoAppState extends State<LogoApp> with SingleTickerProviderStateMixin {
  Animation<double> animation;
  AnimationController controller;

  @override
  void initState() {
    super.initState();
    controller =
        AnimationController(duration: const Duration(seconds: 2), vsync: this);
    animation = CurvedAnimation(parent: controller, curve: Curves.easeIn)
      ..addStatusListener((status) {
        if (status == AnimationStatus.completed) {
          controller.reverse();
        } else if (status == AnimationStatus.dismissed) {
          controller.forward();
        }
      })
      ..addStatusListener((state) => print('$state'));

    controller.forward();
  }

  @override
  Widget build(BuildContext context) => AnimatedLogo(animation: animation);

  @override
  void dispose() {
    controller.dispose();
    super.dispose();
  }
}

class AnimatedLogo extends AnimatedWidget {
  static final _opacityTween = Tween<double>(begin: 0.1, end: 1);
  static final _sizeTween = Tween<double>(begin: 0, end: 300);

  AnimatedLogo({Key key, Animation<double> animation})
      : super(key: key, listenable: animation);

  Widget build(BuildContext context) {
    final animation = listenable as Animation<double>;
    return Center(
      child: Opacity(
        opacity: _opacityTween.evaluate(animation),
        child: Container(
          margin: EdgeInsets.symmetric(vertical: 10),
          height: _sizeTween.evaluate(animation),
          width: _sizeTween.evaluate(animation),
          child: FlutterLogo(),
        ),
      ),
    );
  }
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    Widget titleSection = Container(
      padding: const EdgeInsets.all(32),
      child: Row(
        children: [
          Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Container(
                    padding: const EdgeInsets.only(bottom: 8),
                    child: Text(
                      'This is my PC', style: TextStyle(
                      fontWeight: FontWeight.bold,),
                    ),
                  ),
                  Text("It's so cute, right?", style: TextStyle(
                    color: Colors.grey[500],),
                  ),
                ],
              )
          ),
          FavoriteWidget(),
        ],
      ),
    );
    Color color = Theme.of(context).primaryColor;

    Widget buttonSection = Container(
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          PCWidget(),
          GameWidget(),
          StarWidget(),
        ],
      ),
    );

    Widget textSection = Container(
      padding: const EdgeInsets.all(32),
      child: Text(
        'This is my beautiful PC, and yes it is an absolute beast '
            'of a computer, because it has some awesome specs. It is '
            'currently rocking an AMD Ryzen 7 3700X, a MSI B550 Motherboard, '
            '16GB of GSkill Ripjaws RAM, and last but definitely not least: '
            'a GeForce RTX 2070 Super graphics card. This baby gets me through '
            'even the toughest of days, and can run anything I throw at it like '
            'its NOTHING. Lately, (other than classwork) Ive been using this to '
            'run games such as: Call of Duty, Hell Let Loose, Escape from Tarkov, '
            'Rainbow Six: Siege, and The Elder Scrolls V: Skyrim. My friend asked '
            'me once "Why a PC?", and I just said "Typical Console user."',
        softWrap: true,
      ),
    );

    return MaterialApp(
      title: 'Stateful HW: My PC',
      home: Scaffold(
        appBar: AppBar(
          title: Text('Stateful HW: My PC'),
        ),
        body: ListView(
            children: [
              Image.asset(
                'images/MYPC.jpg',
                width: 240,
                height: 600,
                fit: BoxFit.cover,
              ),
              titleSection,
              buttonSection,
              textSection
            ]
        ),
      ),
    );
  }

}

class FavoriteWidget extends StatefulWidget {
  @override
  _FavoriteWidgetState createState() => _FavoriteWidgetState();
}

class _FavoriteWidgetState extends State<FavoriteWidget> {
  bool _isFavorited = true;
  int _favoriteCount = 23;
  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisSize: MainAxisSize.min,
      children: [
        Container(
          padding: EdgeInsets.all(0),
          child: IconButton(
            icon:(_isFavorited ? Icon(Icons.favorite) : Icon(Icons.favorite_border)),
            color: Colors.red[500],
            onPressed: _toggleFavorite,
          ),
        ),
        SizedBox(
          width: 18,
          child: Container(
            child: Text('$_favoriteCount'),
          ),
        ),
      ],
    );
  }
  void _toggleFavorite() {
    setState(() {
      if (_isFavorited) {
        _favoriteCount -= 1;
        _isFavorited = false;
      }
      else {
        _favoriteCount += 1;
        _isFavorited = true;
      }
    });
  }
}

class PCWidget extends StatefulWidget {
  @override
  _PCWidgetState createState() => _PCWidgetState();
}

class _PCWidgetState extends State<PCWidget> {
  bool _isSelected = true;
  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Container(
          padding: EdgeInsets.all(0),
          child: IconButton(
            icon:(_isSelected ? Icon(Icons.desktop_windows, color: Colors.blue) : Icon(Icons.whatshot)),
            color: Colors.red[500],
            onPressed: _toggleSelected,
          ),
        ),
        if (_isSelected) SizedBox(
          width: 80,
          child: Container(
            padding: EdgeInsets.all(0),
            child:
            Text('My PC',
              style: TextStyle(
                fontSize: 12,
                fontWeight: FontWeight.w400,
                color: Colors.blue,
              ),
              textAlign: TextAlign.center,
            ),
          ),
        ) else SizedBox(
          width: 80,
          child: Container(
            child:
            Text('Its Pretty Fire',
              style: TextStyle(
                fontSize: 12,
                fontWeight: FontWeight.w400,
                color: Colors.red,
              ),
              textAlign: TextAlign.center,

            ),
          ),
        ),
      ],
    );
  }
  void _toggleSelected() {
    setState(() {
      if (_isSelected) {
        _isSelected = false;
      }
      else {
        _isSelected = true;
      }
    });
  }
}

class GameWidget extends StatefulWidget {
  @override
  _GameWidgetState createState() => _GameWidgetState();
}

class _GameWidgetState extends State<GameWidget> {
  bool _isSelected = true;
  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Container(
          padding: EdgeInsets.all(0),
          child: IconButton(
            icon:(_isSelected ? Icon(Icons.videogame_asset, color: Colors.blue) : Icon(Icons.done)),
            color: Colors.green,
            onPressed: _toggleSelected,
          ),
        ),
        if (_isSelected) SizedBox(
          width: 80,
          child: Container(
            padding: EdgeInsets.all(0),
            child:
            Text('Games',
              style: TextStyle(
                fontSize: 12,
                fontWeight: FontWeight.w400,
                color: Colors.blue,
              ),
              textAlign: TextAlign.center,
            ),
          ),
        ) else SizedBox(
          width: 80,
          child: Container(
            child:
            Text('All of them!',
              style: TextStyle(
                fontSize: 12,
                fontWeight: FontWeight.w400,
                color: Colors.green,
              ),
              textAlign: TextAlign.center,

            ),
          ),
        ),
      ],
    );
  }
  void _toggleSelected() {
    setState(() {
      if (_isSelected) {
        _isSelected = false;
      }
      else {
        _isSelected = true;
      }
    });
  }
}

class StarWidget extends StatefulWidget {
  @override
  _StarWidgetState createState() => _StarWidgetState();
}

class _StarWidgetState extends State<StarWidget> {
  bool _isSelected = true;
  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Container(
          padding: EdgeInsets.all(0),
          child: IconButton(
            icon:(_isSelected ? Icon(Icons.star_border, color: Colors.blue) : Icon(Icons.stars)),
            color: Colors.orangeAccent,
            onPressed: _toggleSelected,
          ),
        ),
        if (_isSelected) SizedBox(
          width: 80,
          child: Container(
            padding: EdgeInsets.all(0),
            child:
            Text('Stars?',
              style: TextStyle(
                fontSize: 12,
                fontWeight: FontWeight.w400,
                color: Colors.blue,
              ),
              textAlign: TextAlign.center,
            ),
          ),
        ) else SizedBox(
          width: 80,
          child: Container(
            child:
            Text('Five Stars!!',
              style: TextStyle(
                fontSize: 12,
                fontWeight: FontWeight.w400,
                color: Colors.orangeAccent,
              ),
              textAlign: TextAlign.center,

            ),
          ),
        ),
      ],
    );
  }
  void _toggleSelected() {
    setState(() {
      if (_isSelected) {
        _isSelected = false;
      }
      else {
        _isSelected = true;
      }
    });
  }
}
